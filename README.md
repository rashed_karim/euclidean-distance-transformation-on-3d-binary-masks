# README #

Computes the EDT of the input binary mask

### Usage ###

Usage: 

```
#!python

edt CTxx_la.nii CTxx_edt.nii 
```


### Requirements ###

* IRTK (Image registration toolkit) 

### Who do I talk to? ###

* Rashed Karim (rashed.karim@kcl.ac.uk)