/****************************************************************
*      Author: 
*      Dr. Rashed Karim 
*      Department of Biomedical Engineering, King's College London
*      Email: rashed 'dot' karim @kcl.ac.uk  
*      Copyright (c) 2016
*****************************************************************/

// Makes use of the proprietory Image Registration Toolkit library (IRTK) from Imperial Colllege London. 
#include <irtkImage.h>
#include <irtkEuclideanDistanceTransform.h>

// Other standard C libraries 
#include <fstream>
#include <vector>


void computeEDT(irtkRealImage* img1, irtkRealImage* edt_img) 
{

	irtkRealImage img3; 
	img3 =* img1; 
	irtkRealPixel *p; 
	p = img3.GetPointerToVoxels(); 
	
	// reverse the segmentation mask for EDT
	for (int i=0;i<img3.GetNumberOfVoxels();i++) 
	{
		if (*p>0)
			*p = 1; 
		else
			*p = 0; 
		p++; 
	}

	irtkEuclideanDistanceTransform<irtkRealPixel> *edt = NULL;
	edt = new irtkEuclideanDistanceTransform<irtkRealPixel>(irtkEuclideanDistanceTransform<irtkRealPixel>::irtkDistanceTransform3D);
	edt->SetInput(&img3);
	edt->SetOutput(edt_img);
	edt->Run();
	//edt_img->Write("edt.gipl");
}


int main(int argc, char **argv)
{
	
       char *in_file, *out_file, *in_mask;
       irtkRealImage img1, edt_img; 
       
       irtkRealPixel *ptr1, *ptr2;
       
		if (argc != 3) 
		{
			cout << "\nUsage: edt <input> <output>\n"  
            "The output image is the Euclidean distance from the mask" << endl;
			exit(1); 
		}

       // get input parameters 
      
       in_mask = argv[1]; 
       argv++; 
       argc--; 
       out_file = argv[1]; 
      
       img1.Read(in_mask);
       edt_img = img1; 
       computeEDT(&img1, &edt_img); 
       edt_img.Write(out_file);
}
