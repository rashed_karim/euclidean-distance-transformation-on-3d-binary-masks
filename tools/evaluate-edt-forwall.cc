/****************************************************************
*      Author: 
*      Dr. Rashed Karim 
*      Department of Biomedical Engineering, King's College London
*      Email: rashed 'dot' karim @kcl.ac.uk  
*      Copyright (c) 2016
*****************************************************************/

// Makes use of the proprietory Image Registration Toolkit library (IRTK) from Imperial Colllege London. 
#include "mirtk/Common.h"
#include "mirtk/Options.h"
#include "mirtk/IOConfig.h"
#include "mirtk/GenericImage.h"

#include "mirtk/EuclideanDistanceTransform.h"

// Other standard C libraries 
#include <fstream>
#include <vector>

using namespace mirtk;


/// Type of distance transform
enum DistanceTransformType
{
  DT_Unknown,
  DT_Euclidean,
  DT_CityBlock,
  DT_Laplacian,
  DT_Last
};

void computeEDT(RealImage* img1, RealImage* edt_img) 
{

	RealImage img3; 
	img3 =* img1; 
	RealPixel *p; 
	p = img3.GetPointerToVoxels(); 
	
	// reverse the segmentation mask for EDT
	for (int i=0;i<img3.GetNumberOfVoxels();i++) 
	{
		if (*p>0)
			*p = 1; 
		else
			*p = 0; 
		p++; 
	}

    typedef EuclideanDistanceTransform<RealPixel> EuclideanDistanceTransformType;
    DistanceTransformType type = DT_Euclidean;
    EuclideanDistanceTransformType::Mode euclidean_mode;
    euclidean_mode = EuclideanDistanceTransformType::DT_3D;
    
    EuclideanDistanceTransformType edt(euclidean_mode);

    edt.Input(&img3);
	edt.Output(edt_img);
	edt.Run();
	//edt_img->Write("edt.gipl");
}


int main(int argc, char **argv)
{
	
       char *in_file, *out_file, *in_mask;
       RealImage img1, edt_img; 
       
       RealPixel *ptr1, *ptr2;
       
		if (argc != 3) 
		{
			cout << "\nUsage: edt <input> <output>\n"  
            "The output image is the Euclidean distance from the mask" << endl;
			exit(1); 
		}

       // get input parameters 
      
       in_mask = argv[1]; 
       argv++; 
       argc--; 
       out_file = argv[1]; 
      
       img1.Read(in_mask);
       edt_img = img1; 
       computeEDT(&img1, &edt_img); 
       edt_img.Write(out_file);
}
